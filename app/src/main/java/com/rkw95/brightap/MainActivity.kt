package com.rkw95.brightap

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.provider.Settings
import android.content.Intent
import android.os.Build
import android.view.View
import java.lang.Thread.sleep


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // also ask setting permission at runtime
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val canWriteSettings = Settings.System.canWrite(this)
            if (!canWriteSettings) {
                val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
                startActivity(intent)
            }
        }

        val context: Context = applicationContext


        bttn_brightness.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                // Do some work here
                val normal_brightness = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS)
                val seconds = try {
                    et_seconds.text.toString().toLong()
                } catch (nfe: NumberFormatException) {
                    5
                }
                println("Normal brightness is ${normal_brightness}.")

                // Change the screen brightness change mode to manual.
                Settings.System.putInt(
                    context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
                )
                // Apply the screen brightness value to the system, this will change the value in Settings ---> Display ---> Brightness level.
                // It will also change the screen brightness for the device.

                println("Setting brightness to 255 and sleeping for ${seconds} seconds")
                setBrightness(255)

                sleep(seconds*1000)

                println("Setting brightness back to ${normal_brightness}.")
                setBrightness(normal_brightness)
            }

        })
    }
}

val Context.brightness:Int
    get() {
        return Settings.System.getInt(
            this.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS,
            0
        )
    }

fun Context.setBrightness(value:Int):Unit{
    Settings.System.putInt(
        this.contentResolver,
        Settings.System.SCREEN_BRIGHTNESS,
        value
    )
}